---
name: The Mana World Team (TMWT)
description: Constitution of The Mana World
aliases: [TMWC, TMWT, organization, organisation, org]

autoupdate:
  forums: {forum: 1, topic: 17166, post: 132138}
  wiki: TMW_Team
---

# The Mana World Team (TMWT)

## Abstract
### Duties
- The purpose of the TMWT is to make or delegate decisions regarding the running of The Mana World (TMW).
- There are different scopes under TMWT's authority in order to balance and separate the projects responsibility and management.
- TMWT understands that all members are here to help each other fulfil the common vision of making a Open Source 2D MMORPG.
- Someone can hold part or all of a role and not be a TMWT member, but they need to have the required knowledge or experience to fulfill that role.
- To become a TMWT member, you need to gain in membership in the one of the following categories of contributors; Game Master, Server Admin, or Developer and must be approved by a majority of TMWT.
- In the event that no TMWT members remain, but roles are fulfilled by non-TMWT members, those members may assume TMWT title to fulfill TMWT's duties to the best of their ability.
- The board will be the [SPI](https://spi-inc.org) Liaison or appoint one.

### Issues which require a TMWT vote
- Changes to TMWT governance and/or Organization
- Changes to Game, Forum, Etiquette and Wiki rule changes. (not guidelines)
- Voting a member into TMWT or to adviser status or to revoke membership or ban a member.

### How to conduct a TMWT vote
- A poll must be opened in the ["TMW Team" private section](https://forums.themanaworld.org/viewforum.php?f=23) of the [forums](https://forums.themanaworld.org/).
- The poll will allow re-voting and a running time will be set for 7 days or more.
- Once the first vote has been cast (excluding any vote by the poll author), the poll duration may not be reduced.

### Membership
- If a member becomes inactive (stops) in their contributions for a period longer than 1 year, they will be moved to adviser status.
- Prior members only have to show activity again to regain status.
- Any member can call a vote to revoke someone's membership or ban a member.
- At any time, any member may revoke their own membership or opt for adviser status.

## Contributors
### General Duties
- Maintain guidelines for contributors to follow on the wiki.
- Maintain documentation on changes made.
- Provide tutorials, training and/or time to newer contributors.
- Leadership among the contributors will be done without coercion.
- Addressing community Forums posts / polls / suggestions.

### Game Masters
- Responsible for moderating and enforcing the rules of the game, wiki and forums.
- All Game Masters are members of The Mana World Team.
- To become a Game Master, a person must be voted in by the player community

### Server Admins
- Are those that have ssh access to the server and are responsible for server restarts, maintenance, and the general health of the server overall.
- Are those that are Forum Admins and/or Wiki Admins.
- Are those that deal with Account resets/info and aid in GM investigations.
- To become a member of this group, a person must first show the appropriate knowledge and willingness to devote time.

### Developers
- Programmers are responsible for making changes to the code base.
- Content Creators are responsible for adding quests, items and monsters either via scripting, mapping, sound, client theme or pixel art.
- Web developers are responsible for the website's front and/or back-end work.
- Translators work on client, server and wiki data to provide them in as many languages as we can.
- Developer group maintains a visible project plan.
- To become a member of this group, a person must show appropriate knowledge, contribute to the project and learn and gain 'git' access.

### Advisers
- To become a member of this group, a person needs to be a prior contributor and TMWT member.
- No longer actively contribute.
- Allowed to vote/post as TMWT.


<!-- The section below only contains links to relevant user groups and is not part of the constitution -->
<br><hr>

## Members
- [The Mana World Team](https://forums.themanaworld.org/memberlist.php?mode=group&g=981)
  - [Administrators](https://forums.themanaworld.org/memberlist.php?mode=group&g=962)
  - [Developers](https://forums.themanaworld.org/memberlist.php?mode=group&g=979)
  - [Game Masters](https://forums.themanaworld.org/memberlist.php?mode=group&g=973)
- [The Mana World Advisers](https://forums.themanaworld.org/memberlist.php?mode=group&g=984)
  - [Advisers](https://forums.themanaworld.org/memberlist.php?mode=group&g=984)
